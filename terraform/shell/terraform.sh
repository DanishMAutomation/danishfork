#!/bin/bash

yum -y install unzip

# Install Terraform

if [[ ! -e /var/tmp/terraform_0.11.3_linux_amd64.zip ]]
then
	wget https://releases.hashicorp.com/terraform/0.11.3/terraform_0.11.3_linux_amd64.zip -O /var/tmp/terraform_0.11.3_linux_amd64.zip
fi

if [[ ! -e /usr/local/bin/terraform ]]
then
	cd /usr/local/bin
	unzip /var/tmp/terraform_0.11.3_linux_amd64.zip
fi

cd $HOME
rm -rf /home/vagrant/terraform
cp -r /vagrant/files/terraform /home/vagrant/terraform
chown -R vagrant:vagrant /home/vagrant/terraform

[[ ! -d /home/vagrant/.aws ]] && mkdir /home/vagrant/.aws
cp /vagrant/secrets/credentials /home/vagrant/.aws/credentials
chown -R vagrant:vagrant /home/vagrant/.aws
