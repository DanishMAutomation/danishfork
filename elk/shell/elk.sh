#!/bin/bash

yum -y install java nano

yum -y install \
https://artifacts.elastic.co/downloads/logstash/logstash-6.1.3.rpm \
https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.1.3.rpm \
https://artifacts.elastic.co/downloads/kibana/kibana-6.1.3-x86_64.rpm
