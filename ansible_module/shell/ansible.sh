#!/bin/bash

cd /vagrant/ansible

if [[ -e /etc/redhat-release ]]
then
  yum -y install ansible
else
  apt-get -y update
  apt-get -y install software-properties-common
  apt-add-repository -y ppa:ansible/ansible
  apt-get -y update
  apt-get -y install ansible sshpass
fi

ansible-playbook -i inventory site.yml --limit webservers

# Test our page worked
if curl -s http://localhost/status.html | grep ANSIBLE >/dev/null 2>&1
then
  echo "Web page working correctly"
else
  echo "Failed to get web page"
  exit 1
fi
