#!/bin/bash

yum -y install httpd
systemctl enable httpd
systemctl start httpd

cp /vagrant/files/webscript /etc/init.d
chmod +x /etc/init.d/webscript
chkconfig --add webscript
service webscript start
# systemctl start webscript
