#!/bin/bash

# Install docker software from community
yum -y install  yum-utils  device-mapper-persistent-data lvm2

# Install consul-template
cd /bin
tar xvf /vagrant/files/consul-template.tgz

# Set up docker software repository to allow install of docker software
yum-config-manager -y --add-repo https://download.docker.com/linux/centos/docker-ce.repo

# Install Docker community edition
yum -y install docker-ce

# systemctl start docker
service docker start

# systemctl enable docker
chkconfig docker on

# Add vagrant user to docker group
usermod -G docker vagrant

myRealIP=$(ifconfig | grep -A6 enp0s8 | grep 'inet ' | awk '{print $2}')
dockerIP=$(ifconfig | grep -A6 docker0 | grep 'inet ' | awk '{print $2}')

# Start a consul server
docker run -d -h consulnode1 -v /mnt:/data \
-p $myRealIP:8300:8300 \
-p $myRealIP:8301:8301 \
-p $myRealIP:8301:8301/udp \
-p $myRealIP:8302:8302 \
-p $myRealIP:8302:8302/udp \
-p $myRealIP:8400:8400 \
-p $myRealIP:8500:8500 \
-p $dockerIP:53:53/udp \
progrium/consul -server -advertise $myRealIP -bootstrap

# Start the registrator
docker run -d \
-v /var/run/docker.sock:/tmp/docker.sock \
--name registrator -h registrator \
gliderlabs/registrator:latest consul://$myRealIP:8500

# Start an application
docker run -d -P --name webnode1 -h webnode1 -e "SERVICE_TAGS=webserver" httpd

# Add HAProxy
yum -y install haproxy
cp /vagrant/files/haproxy.ctmplt /etc/haproxy/
cp /vagrant/files/haproxy.cfg /etc/haproxy/
service haproxy start
chkconfig haproxy on

cp /vagrant/bin/update-proxy /etc/init.d
chkconfig --add update-proxy
service update-proxy start
