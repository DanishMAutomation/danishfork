#!/bin/bash

echo "STARTING basic EC2 instance"
output=$(aws ec2 run-instances --key-name lsd --subnet-id subnet-c1b2ba8b --image-id ami-8ee056f3 --instance-type t2.micro --region eu-west-3 --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=lsdPC}]')
INSTID=$(echo "$output" | jq '.Instances[].InstanceId' | sed 's/"//g')
HOSTIP=$(echo "$output" | jq '.Instances[].NetworkInterfaces[].PrivateDnsName' | sed 's/"//g')

echo "Instance ID = $INSTID"
echo "Private DNS = $HOSTIP"

echo "WAITING for EC2 instance to be ready"
until ssh ec2-user@$HOSTIP 'hostname'
do
	sleep 10
    echo -n '.'
done

echo "Copy artifact to system"
aws s3 cp s3://vazbucket/lsd/ /var/lib/jenkins/workspace/petclinic/target/ --recursive --exclude '*' --include '*.jar'
scp /var/lib/jenkins/workspace/petclinic/target/spring-petclinic-*.jar ec2-user@$HOSTIP:

echo "Put the artifact in its own dir"
ssh ec2-user@$HOSTIP 'sudo mkdir /opt/petclinic && sudo mv /home/ec2-user/spring-petclinic*.jar /opt/petclinic'

echo "Installing necessary software"
ssh ec2-user@$HOSTIP 'sudo yum -y erase java-1.7.0-openjdk'
ssh ec2-user@$HOSTIP 'sudo yum -y install epel java-1.8.0-openjdk mysql'
ssh ec2-user@$HOSTIP 'sudo yum -y install https://download1.rpmfusion.org/free/el/rpmfusion-free-release-7.noarch.rpm https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-7.noarch.rpm'

echo "Copy an RC script onto the server to enable stop/start"
cat >/tmp/petclinic <<_END_
#!/bin/bash

#description: Petclinic control script
#chkconfig: 2345 99 99

case \$1 in
  'start')
    # The next 2 lines is only required if you want to compile and run
    #cd /opt/spring-petclinic
    #./mvnw spring-boot:run >/var/log/petclinic.stdout 2>/var/log/petclinic.stderr &
    # The next 2 lines are for running PC from a pre-compiled jar
    cd /opt/petclinic
    java -jar /opt/petclinic/spring-petclinic-2.0.0.jar >/var/log/petclinic.stdout 2>/var/log/petclinic.stderr &
    ;;
  'stop')
    kill \$(ps -ef | grep petclinic | grep -v grep | awk '{print \$2}')
    ;;
  'status')
    PID=\$(ps -ef | grep java | grep petclinic | grep -v grep | awk '{print \$2}')
    if [[ -n \$PID ]]
    then
      echo "Petclinic is running with PID \$PID"
    fi
    ;;
  *)
    echo "I do not understand that option"
    ;;
esac
_END_

scp /tmp/petclinic ec2-user@$HOSTIP:
ssh ec2-user@$HOSTIP 'chmod +x petclinic'
ssh ec2-user@$HOSTIP 'sudo mv petclinic /etc/init.d'
ssh ec2-user@$HOSTIP 'sudo chkconfig --add petclinic'
ssh ec2-user@$HOSTIP 'sudo chkconfig petclinic on'

rm /tmp/petclinic

echo "Starting Petclinic from artifact"
ssh ec2-user@$HOSTIP 'sudo yum update'
ssh ec2-user@$HOSTIP 'sudo service petclinic start'

echo "Create the new AMI for petclinic"
output=$(aws ec2 create-image --instance-id $INSTID --name 'PCtest' --region eu-west-3)
ImageID=$(echo "$output" | jq '.ImageId' | sed 's/"//g')
echo "Image ID = $ImageID"
until aws ec2 describe-images --image-ids $ImageID --region eu-west-3 | jq '.Images[].State' | sed 's/"//g' | grep available
do
	sleep 20
    echo -n '.'
done

echo "Delete instance"
aws ec2 terminate-instances --instance-ids $INSTID --region eu-west-3

#cat >~/var/lib/jenkins/workspace/update/ami <<_END_
#newAMI = $ImageID
#_END_

tag=lsd
if ! aws cloudformation update-stack \
--stack-name ${tag}Instances \
--use-previous-template \
--region eu-west-3 \
--parameters \
ParameterKey=subnetA,UsePreviousValue=true \
ParameterKey=subnetB,UsePreviousValue=true \
ParameterKey=sgId,UsePreviousValue=true \
ParameterKey=tag,UsePreviousValue=true \
ParameterKey=amiId,ParameterValue=$ImageID \
--tags "Key=Name,Value=${tag}Instances"; then
  echo "Update failed, check tags"
fi
